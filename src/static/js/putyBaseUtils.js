//弹窗
export const alert = (msg) => {
	uni.showModal({
		showCancel: false,
		title: msg + ""
	})
}

//显示加载框
export const showLoad = (msg) => {
	uni.showLoading({
		title: msg ? msg : "加载中...",
		mask: true
	})
}

//关闭加载框
export const hideLoad = () => {
	uni.hideLoading();
}
