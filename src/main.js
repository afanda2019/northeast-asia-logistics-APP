import Vue from 'vue'
import App from './App'
import uView from "uview-ui";

import {URL,ajaxPost, getTokens, toDs, gTo, cTo, _toast, setHeight, getHeight, backT, _call, checkPhone, _confirm, isNull, _upLoadPic,
 _alert, replaceDetail, getSync, setSync, _upLoadVideo, removeSync, getParam, inputBlur,screenshot,showLoading,closeLoading,
 browseFinish,throttle,noMultipleClicks, sTo, _pay, _getLocation, testIdCard,previewImage,previewImageSA,_delImage, } from "./static/js/common";

Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()

Vue.config.productionTip = false;
Vue.prototype.$url = URL;
Vue.prototype.$ajax = ajaxPost;
Vue.prototype.$getTokens = getTokens;
Vue.prototype.$toDs = toDs;
Vue.prototype.$gTo = gTo;
Vue.prototype.$cTo = cTo;
Vue.prototype.$toast = _toast;
Vue.prototype.$setHeight = setHeight;
Vue.prototype.$getHeight = getHeight;
Vue.prototype.$backT = backT;
Vue.prototype.$call = _call;
Vue.prototype.$checkPhone = checkPhone;
Vue.prototype.$confirm = _confirm;
Vue.prototype.$isNull = isNull;
Vue.prototype.$upLoadPic = _upLoadPic;
Vue.prototype.$alert = _alert;
Vue.prototype.$replaceDetail = replaceDetail;
Vue.prototype.$getSync = getSync;
Vue.prototype.$setSync = setSync;
Vue.prototype.$upLoadVideo = _upLoadVideo;
Vue.prototype.$removeSync = removeSync;
Vue.prototype.$getParam = getParam;
Vue.prototype.$inputBlur = inputBlur;
Vue.prototype.$screenshot = screenshot;
Vue.prototype.$showLoading = showLoading;
Vue.prototype.$closeLoading = closeLoading;
Vue.prototype.$browseFinish = browseFinish;
Vue.prototype.$throttle = throttle;
Vue.prototype.$noMultipleClicks = noMultipleClicks;
Vue.prototype.$sTo = sTo;
Vue.prototype.$pay = _pay;
Vue.prototype.$getLocation = _getLocation;
Vue.prototype.$testIdCard = testIdCard;
Vue.prototype.$previewImage = previewImage;
Vue.prototype.$previewImageSA = previewImageSA;
Vue.prototype.$delImage = _delImage;





